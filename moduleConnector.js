// CONFIG
const config = {
    API_URL: "http://api-matice.marju-preprod.ovh/",
    AUTH_FIELDS: {
        USERNAME: "login-usr",
        PASSWORD: "login-pwd",
        FORM: "login-form"
    }
};


// DEPENDENCIES : jQuery
(function () {
    // Load the script
    var script = document.createElement("SCRIPT");
    script.src = '../bower_components/jquery/dist/jquery.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existance
    var checkReady = function (callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function () {
                checkReady(callback);
            }, 20);
        }
    };

    // Start polling...
    checkReady(function ($) {
        $(function () {
            console.log('jQuery Loaded !');
            init();
            function init() {
                initLoader();
                var key = retrieveKey();
                if (key) {
                    showLoader();
                    getCredentials(key, function (res) {
                        console.log(res);
                        if (!res) {
                            hideLoader();
                        } else {
                            $('#' + config.AUTH_FIELDS.USERNAME).val(res.context.username);
                            $('#' + config.AUTH_FIELDS.PASSWORD).val(res.context.password);
                            $('#' + 'connectionSuccess').text("connecté avec le compte : " + res.context.username);
                            $('.center-wrapper').css('display','none');
                            $('#testConnection').css('display','block');
                            hideLoader();
                            //$('#' + config.AUTH_FIELDS.FORM).submit();
                            console.log(JSON.stringify(res.context));
                        }
                    });
                } else {
                    console.log('...');
                }

            }


            function initLoader() {
                var loaderStyle = {
                    "margin": "auto",
                    "border": "16px solid #f3f3f3", /* Light grey */
                    "border-top": "16px solid #3498db", /* Blue */
                    "border-radius": "50%",
                    "width": "120px",
                    "height": "120px",
                    "animation": "spin 2s linear infinite"
                };
                var wrapperStyle = {
                    "position": "absolute",
                    "top": "50vh",
                    "left": "50vw",
                    "transform": "translatex(-50%)",
                    "width": "100vw"
                };
                var backdropStyle = {
                    "display": "none",
                    "position": "absolute",
                    "top": "0",
                    "left": "0",
                    "width": "100vw",
                    "height": "100vh",
                    "z-index": "10000",
                    "background-color": "rgba(255,255,255,0.8)"
                };
                var style = document.createElement('style');
                style.type = 'text/css';
                style.innerHTML = '\
                    @-webkit-keyframes spin {\
                        0% {\
                         -webkit-transform: rotate(0deg);\
                         }\
                        100% {\
                         -webkit-transform: rotate(360deg);\
                         }\
                    }\
                    @-moz-keyframes spin {\
                        0% {\
                     -webkit-transform: rotate(0deg);\
                        }\
                       100% {\
                     -webkit-transform: rotate(360deg);\
                        }\
                    }';
                document.getElementsByTagName('head')[0].appendChild(style);

                $("body").append("<div class='global-backdrop'><div class='connector-wrapper'><div class='connector-loader'></div></div></div>");
                $('.global-backdrop').css(backdropStyle);
                $('.connector-wrapper').css(wrapperStyle);
                $('.connector-loader').css(loaderStyle);
            }

            function showLoader() {
                $('.global-backdrop').css('display', 'block');
            }

            function hideLoader() {
                $('.global-backdrop').css('display', 'none');
            }


            function retrieveKey() {
                return (getUrlParameter('accessKey') || false);
            }

            function getCredentials(accessKey, callback) {
                var req = $.post(config.API_URL + "plugins/askForPluginContext", {accessKey: accessKey}, function () {
                }).done(function (res) {
                    if (res.success) {
                        console.log('success');
                        callback(res);
                    } else {
                        console.log('nope');
                        callback(false);
                    }
                }).fail(function () {
                    console.log("error");
                    callback(false);
                }).always(function () {
                    console.log("finished");
                });
            }

            function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }

        });
    });
})();